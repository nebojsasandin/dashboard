import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { DataService } from '../../_services/data.service';
import { BaseRequestOptions, Http, RequestMethod, RequestOptions, Response, ResponseOptions, XHRBackend } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { Configuration } from '../../app.constants';
// import { GlobalEx } from '../../globals';
// import { window } from '../../app.component';
import { GlobalVars } from '../../app.component';
// import 'global' from '../../app.component';
// import { Configuration } from '../../app.constants';
// import { ToasterService } from 'angular2-toaster/angular2-toaster';
// import myGlobals = require('../../globals');
// import * as myGlobals from '../../globals';

// declare var DataService: typeof DataService;
// declare let DataService: any;

// declare let global: Global;

export function mockBackEndFactory(backend: MockBackend, options: BaseRequestOptions, realBackend: XHRBackend) {
    // array in local storage for registered users
    //let users: any[] = JSON.parse(localStorage.getItem('users')) || [];

    let users: any[] = [];
    //GlobalVars.WCFService.GetGlobalData<any>('getusers').subscribe((data: any[]) => {
    //    users = data;
    //});

    // fake token
    let token: string = 'fake-jwt-token';// localStorage.getItem("jwttoken");//'fake-jwt-token';// localStorage.getItem("jwttoken");//'angular-jwt-token';//fake-jwt-token';

    //let message: string = "empty";//'empty';

    // let _dataService: DataService = new DataService(this.Http, new Configuration());

    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
        // wrap in timeout to simulate server api call
        //setTimeout(() => {
            
            // authenticate
            if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                // get parameters from post request
                let params = JSON.parse(connection.request.getBody());

                //let connection2 = connection;
                // find if any user matches login credentials
                let loggedIn = false;
                GlobalVars.WCFService.PostGlobalData<any>('authenticate', params)
                .subscribe(
                    (data: any) => {
                        let user = data;
                        if (user.error == true) {
                            connection.mockError(new Error(user.reason));
                        }
                        else {
                            localStorage.setItem('user', JSON.stringify(user));
                            GlobalVars.CurrentUser = user;
                            //localStorage.setItem('campID', String(user.campid));
                            //localStorage.setItem("apikey", String(user.apikey));
                            localStorage.setItem("jwttoken", String(user.jwttoken));
                            token = user.jwttoken;
                            loggedIn = true;
                            console.log("logged in as " + user.fullname);
                            connection.mockRespond(new Response(new ResponseOptions({
                                status: 200,
                                body: {
                                    id: user.id,
                                    fullname: user.fullname,
                                    email: user.email,
                                    token: 'fake-jwt-token',
                                    campid: user.campid,
                                    apikey: user.apikey,
                                }
                            })));
                        }
                    },
                    error => {
                        connection.mockError(new Error('Email or password is incorrect'));
                    }
                );

                //let filteredUsers = users.filter(user => {
                //    return user.email === params.email && user.password === params.password;
                //});

                //// default account
                //if (params.email === 'briandean@gmail.com' && params.password === 'briandean') {
                //    filteredUsers[0] = {
                //        fullName: 'Brian Dean',
                //        email: 'briandean@gmail.com',
                //        password: 'briandean',
                //        campid: 95,
                //        apikey: '2c21db67-ce44-4918-95c7-841fdbca439c',
                //    };
                //    localStorage.setItem("campID", "95");
                //    localStorage.setItem("apikey", "2c21db67-ce44-4918-95c7-841fdbca439c");

                //    GlobalVars.WCFService
                //    // myGlobals.DataService
                //    // // GlobalEx.DataService
                //    // // // global.DataService
                //        .GetData<any>('sometest', 95)
                //        .subscribe(
                //            (data: any) => {
                //                let tempString = String(data.retVal);
                //                message = tempString;
                //            },
                //            error => (err) => {
                //                console.log('error: ' + err);
                //            },
                //            () => {
                //                console.log('success: ' + message);
                //            }
                //        );

                //    // _dataService
                //    //     .getSingle<string>('sometest', 95)
                //    //     .subscribe(
                //    //         (data: string) => message = data,
                //    //         error => () => {
                //    //             console.log('error');
                //    //         },
                //    //         () => {
                //    //             console.log('success: ' + message);
                //    //         }
                //    //     );
                //}

                //if (filteredUsers.length) {
                //    // if login details are valid return 200 OK with user details and fake jwt token
                //    let user = filteredUsers[0];
                //    localStorage.setItem('campID', String(user.campid));
                //    localStorage.setItem("apikey", String(user.apikey));
                //    connection.mockRespond(new Response(new ResponseOptions({
                //        status: 200,
                //        body: {
                //            id: user.id,
                //            fullName: user.fullName,
                //            email: user.email,
                //            token: token,
                //            campid: user.campid,
                //            apikey: user.apikey,
                //        }
                //    })));
                //} else {
                //    // else return 400 bad request
                //    connection.mockError(new Error('Email or password is incorrect'));
                //}

                return;
            }

            // get users
            if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return users if valid, this security
                // is implemented server side in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer ' + token) {
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: users })));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }

                return;
            }

            // get user by id
            if (connection.request.url.match(/\/api\/users\/\d+$/) && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer ' + token) {
                    // find user by id in users array
                    let urlParts = connection.request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedUsers = users.filter(user => {
                        return user.id === id;
                    });
                    let user = matchedUsers.length ? matchedUsers[0] : null;

                    // respond 200 OK with user
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: user })));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }

                return;
            }

            //// create user
            //if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Post) {
            //    // get new user object from post body
            //    let newUser = JSON.parse(connection.request.getBody());

            //    // validation
            //    let duplicateUser = users.filter(user => {
            //        return user.email === newUser.email;
            //    }).length;
            //    if (duplicateUser) {
            //        return connection.mockError(new Error('Email "' + newUser.email + '" is already registered'));
            //    }

            //    // save new user
            //    newUser.id = users.length + 1;
            //    users.push(newUser);
            //    localStorage.setItem('users', JSON.stringify(users));

            //    // respond 200 OK
            //    connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));

            //    return;
            //}

            //// delete user
            //if (connection.request.url.match(/\/api\/users\/\d+$/) && connection.request.method === RequestMethod.Delete) {
            //    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
            //    if (connection.request.headers.get('Authorization') === 'Bearer ' + token) {
            //        // find user by id in users array
            //        let urlParts = connection.request.url.split('/');
            //        let id = parseInt(urlParts[urlParts.length - 1]);
            //        for (let i = 0; i < users.length; i++) {
            //            let user = users[i];
            //            if (user.id === id) {
            //                // delete user
            //                users.splice(i, 1);
            //                localStorage.setItem('users', JSON.stringify(users));
            //                break;
            //            }
            //        }

            //        // respond 200 OK
            //        connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));
            //    } else {
            //        // return 401 not authorised if token is null or invalid
            //        connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
            //    }

            //    return;
            //}

            // token verify
            if (connection.request.url.endsWith('/api/verify') && connection.request.method === RequestMethod.Get) {
                // check for fake auth token in header and return users if valid, this security
                // is implemented server side in a real application
                if (connection.request.headers.get('Authorization') === 'Bearer ' + token) {
                    let user = JSON.parse(localStorage.getItem("user"));
                    GlobalVars.CurrentUser = user;
                    connection.mockRespond(new Response(new ResponseOptions({ status: 200, body: { status: 'ok' } })));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(new ResponseOptions({ status: 401 })));
                }

                return;
            }

            //// forgot password
            //if (connection.request.url.endsWith('/api/forgot-password') && connection.request.method === RequestMethod.Post) {
            //    // get parameters from post request
            //    let params = JSON.parse(connection.request.getBody());

            //    // find if any user matches login credentials
            //    let filteredUsers = users.filter(user => {
            //        return user.email === params.email;
            //    });

            //    if (filteredUsers.length) {
            //        // in real world, if email is valid, send email change password link
            //        let user = filteredUsers[0];
            //        connection.mockRespond(new Response(new ResponseOptions({ status: 200 })));
            //    } else {
            //        // else return 400 bad request
            //        connection.mockError(new Error('User with this email does not exist'));
            //    }

            //    return;
            //}

            // pass through any requests not handled above
            let realHttp = new Http(realBackend, options);
            let requestOptions = new RequestOptions({
                method: connection.request.method,
                headers: connection.request.headers,
                body: connection.request.getBody(),
                url: connection.request.url,
                withCredentials: connection.request.withCredentials,
                responseType: connection.request.responseType
            });
            realHttp.request(connection.request.url, requestOptions)
                .subscribe((response: Response) => {
                    connection.mockRespond(response);
                },
                (error: any) => {
                    connection.mockError(error);
                });

        //}, 500);

    });

    return new Http(backend, options);
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    deps: [MockBackend, BaseRequestOptions, XHRBackend],
    useFactory: mockBackEndFactory
};