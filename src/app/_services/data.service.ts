import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { catchError, tap } from 'rxjs/operators';

import { HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Configuration } from '../../app/app.constants';

const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

@Injectable()
export class DataService {

    private actionUrl: string;

    private handleError(operation: String) {
        return (err: any) => {
            let errMsg = `error in ${operation}() retrieving ${this.actionUrl}`;
            console.log(`${errMsg}:`, err)
            if (err instanceof HttpErrorResponse) {
                // you could extract more info about the error if you want, e.g.:
                console.log(`status: ${err.status}, ${err.statusText}`);
                // errMsg = ...
            }
            return Observable.throw(errMsg);
        }
    }

    constructor(private http: HttpClient, private _configuration: Configuration) {
        this.actionUrl = _configuration.ServerWithApiUrl;
    }

    public GetData<T>(command: string, campID: number = 0): Observable<T> {
        let fullUrl: string = this.actionUrl + "get/" + JSON.parse(localStorage.getItem("user")).apikey + "/" + command + "/" + campID;
        let retVal = this.http.get<T>(fullUrl);
        return retVal.pipe(tap(data => /*console.log(data)*/ { }), catchError(this.handleError('GetData')));
    }

    public PostData<T>(command: string, data: any, campID: number = 0): Observable<T> {
        let fullUrl: string = this.actionUrl + "post/" + JSON.parse(localStorage.getItem("user")).apikey + "/" + command + "/" + campID;
        let retVal = this.http.post<T>(fullUrl, JSON.stringify(data), { headers: headers });
        return retVal.pipe(tap(data => /*console.log(data)*/ { }), catchError(this.handleError('PostData')));
    }

    public PostDataNoRet(command: string, data: any, campID: number = 0) {
        let fullUrl: string = this.actionUrl + "post/" + JSON.parse(localStorage.getItem("user")).apikey + "/" + command + "/" + campID;
        this.http.post(fullUrl, JSON.stringify(data), { headers: headers }).pipe(tap(data => /*console.log(data)*/ { }), catchError(this.handleError('PostData')));
    }

    public GetGlobalData<T>(command: string): Observable<T> {
        let fullUrl: string = this.actionUrl + "global/key=476a1e02-cc3c-4d96-b55b-2604329a60b7/get/" + command;
        let retVal = this.http.get<T>(fullUrl);
        return retVal.pipe(tap(data => /*console.log(data)*/ { }), catchError(this.handleError('GetGlobalData')));
    }

    public PostGlobalData<T>(command: string, data: any): Observable<T> {
        let fullUrl: string = this.actionUrl + "global/key=476a1e02-cc3c-4d96-b55b-2604329a60b7/post/" + command;
        let retVal = this.http.post<T>(fullUrl, JSON.stringify(data), { headers: headers });
        return retVal.pipe(tap(data => /*console.log(data)*/ { }), catchError(this.handleError('PostGlobalData')));
    }
}

@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }

        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        //console.log(JSON.stringify(req.headers));
        return next.handle(req);
    }
}
