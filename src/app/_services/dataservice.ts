import 'rxjs/add/operator/map';

import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Configuration } from '../../app/app.constants';

@Injectable()
export class DataService {

    private actionUrl: string;

    constructor(private http: HttpClient, private _configuration: Configuration) {
        this.actionUrl = _configuration.ServerWithApiUrl;
    }

    public getAll<T>(command: string, campID: number = 0): Observable<T> {
        let fullUrl: string = this.actionUrl + command;
        if (campID > 0) {
            fullUrl = fullUrl + "/" + campID;
        }
        return this.http.get<T>(fullUrl);
    }

    public getSingle<T>(command: string, campID: number = 0): Observable<T> {
        let fullUrl: string = this.actionUrl + command;
        if (campID > 0) {
            fullUrl = fullUrl + "/" + campID;
        }
        return this.http.get<T>(fullUrl);
    }

    // public add<T>(itemName: string): Observable<T> {
    //     const toAdd = JSON.stringify({ ItemName: itemName });

    //     return this.http.post<T>(this.actionUrl, toAdd);
    // }

    // public update<T>(command: string, id: number, itemToUpdate: any): Observable<T> {
    //     return this.http
    //         .put<T>(this.actionUrl + command + "/" + id, JSON.stringify(itemToUpdate));
    // }

    // public delete<T>(id: number): Observable<T> {
    //     return this.http.delete<T>(this.actionUrl + id);
    // }
}


@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }

        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        console.log(JSON.stringify(req.headers));
        return next.handle(req);
    }
}