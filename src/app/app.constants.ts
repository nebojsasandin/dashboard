import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public Server = window.location.protocol + '//' + window.location.hostname + ":17022/";//'http://localhost::17022/';
    public ApiUrl = 'DMCAProtectionWebAPI/dashboardapi/';
    public ServerWithApiUrl = this.Server + this.ApiUrl;
}