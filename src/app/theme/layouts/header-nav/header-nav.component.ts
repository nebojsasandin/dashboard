import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { GlobalVars } from '../../../../app/app.component';

declare let mLayout: any;

@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

    public loggeduser2: any;

    constructor() {

    }
    ngOnInit() {
        this.loggeduser2 = GlobalVars.CurrentUser;// JSON.parse(localStorage.getItem("user"));
        //loggeduser2 = loggeduser;
    }
    ngAfterViewInit() {

        mLayout.initHeader();

    }

}