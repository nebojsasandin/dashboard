import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { GlobalVars } from '../../../../../../app/app.component';


@Component({
    selector: "app-header-profile",
    templateUrl: "./header-profile.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderProfileComponent implements OnInit {

    public loggeduser2: any;
    public clientprofile: any;

    constructor() {

    }
    ngOnInit() {
        this.loggeduser2 = GlobalVars.CurrentUser;// JSON.parse(localStorage.getItem("user"));

        GlobalVars.WCFService.GetData<any>('getclientprofile', this.loggeduser2.campid)
            .subscribe((data: any) => {
                GlobalVars.ClientProfile = data;
                this.clientprofile = data;
            });
    }

}