import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
//import { ChartsMorrisChartsComponent } from '../../../../../../src/app/theme/pages/default/components/charts/charts-morris-charts/charts-morris-charts.component';
import { ChartsGoogleChartsComponent } from '../../../../../../src/app/theme/pages/default/components/charts/charts-google-charts/charts-google-charts.component';
//import { ChartsMorrisChartsModule } from '../components/charts/charts-morris-charts/charts-morris-charts.module';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": IndexComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule
    ], exports: [
        RouterModule
    ], declarations: [
        ChartsGoogleChartsComponent, /*ChartsMorrisChartsComponent,*/ IndexComponent
    ]
})
export class IndexModule {



}