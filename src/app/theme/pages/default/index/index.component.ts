import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
//import { ChartsMorrisChartsComponent } from '../../../../../../src/app/theme/pages/default/components/charts/charts-morris-charts/charts-morris-charts.component';
//import { ChartsMorrisChartsModule } from '../components/charts/charts-morris-charts/charts-morris-charts.module';
import { GlobalVars } from '../../../../../app/app.component';//app.component';
import { AlertComponent } from '../../../../auth/_directives';

declare var $: any;
declare var Morris: any;
declare var google: any;

@Component({
    selector: "app-index",
    templateUrl: "./index.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit, AfterViewInit {

    public loggeduser2: any;

    constructor(private _script: ScriptLoaderService) {

    }
    ngOnInit() {

        this.loggeduser2 = GlobalVars.CurrentUser;
        //var e = document.createElement('div');
        //e.setAttribute("id", "please_wait_el");
        //e.innerHTML = "<span style=\"font - size: 24px;\">Please wait... Data is loading...</span>";
        //var parent = document.getElementById('chart1part');
        //parent.appendChild(e);
        //var el: HTMLElement = document.getElementById('m_morris_2');

        //$(window).on('resize', function () {
        //    if (!window.recentResize) {
        //        window.m.redraw();
        //        window.recentResize = true;
        //        setTimeout(function () { window.recentResize = false; }, 200);
        //    }
        //});

        //GlobalVars.WCFService.GetData<any>('getchart/type1')
        //.subscribe((data: any) => {
        //    if (data.error == true) {
        //        //connection.mockError(new Error(user.reason));
        //    }
        //    else {
        //        var m_morris_2 = Morris.Area(data);
        //        m_morris_2.options.labels.forEach(function (label, i) {
        //            var legendItem = $('<span></span>').text(label).prepend('<i>&nbsp;</i>');
        //            legendItem.find('i').css('backgroundColor', m_morris_2.options.lineColors[i]);
        //            $('#m_legend').append(legendItem)
        //        });
        //        document.getElementById("please_wait_el").remove();
        //    }
        //},
        //    error => {
        //        //connection.mockError(new Error('Email or password is incorrect'));
        //    }
        //);

        GlobalVars.WCFService.GetData<any>('getchart/type2', this.loggeduser2.campid)
            .subscribe((data: any) => {
                //var m_morris_2 = Morris.Area(data);
                //m_morris_2.options.labels.forEach(function (label, i) {
                //    var legendItem = $('<span></span>').text(label).prepend('<i>&nbsp;</i>');
                //    legendItem.find('i').css('backgroundColor', m_morris_2.options.lineColors[i]);
                //    $('#m_legend').append(legendItem)
                //});

                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(function () {
                    var tempData = google.visualization.arrayToDataTable(data.chart1);
                    var options = {
                        //title: 'Google Page Numbers Breakdown',
                        pieHole: 0.4,
                        pieStartAngle: 270,
                        chartArea: { /*left: 30, top: 10,*/ width: '80%', height: '80%' },
                        legend: { position: 'right', alignment: 'center' }
                        //legend: {
                        //    position: { position: 'top', alignment: 'end' },
                        //    labeledValueText: 'both',
                        //}
                    };
                    var chart = new google.visualization.PieChart(document.getElementById('m_gchart_4'));
                    chart.draw(tempData, options);
                    document.getElementById("please_wait_el").remove();

                    var tempData2 = google.visualization.arrayToDataTable(data.chart2);
                    var options2 = {
                        //title: 'Google Page Numbers Breakdown',
                        //pieHole: 0.4,
                        pieStartAngle: 270,
                        chartArea: { /*left: 30, top: 10,*/ width: '74%', height: '74%' },
                        //legend: { position: 'right', alignment: 'center' }
                        legend: { position: 'top', alignment: 'center' }
                        //legend: {
                        //    position: { position: 'top', alignment: 'end' },
                        //    labeledValueText: 'both',
                        //}
                    };
                    var chart2 = new google.visualization.PieChart(document.getElementById('m_gchart_5'));
                    chart2.draw(tempData2, options2);
                    document.getElementById("please_wait_el2").remove();

                    var tempData3 = google.visualization.arrayToDataTable(data.chart3);
                    var options3 = {
                        vAxis: { title: 'Number of Links' },
                        hAxis: { title: 'Date' },
                        seriesType: 'bars',
                        series: { 1: { type: 'line' } },
                        //title: 'Google Page Numbers Breakdown',
                        //pieHole: 0.4,
                        //pieStartAngle: 270,
                        chartArea: { /*left: 30, top: 10,*/ width: '80%', height: '80%' },
                        legend: 'none'
                        
                        //legend: { position: 'right', alignment: 'center' }
                        //legend: {
                        //    position: { position: 'top', alignment: 'end' },
                        //    labeledValueText: 'both',
                        //}
                    };
                    var chart3 = new google.visualization.ComboChart(document.getElementById('m_gchart_6'));
                    chart3.draw(tempData3, options3);
                    document.getElementById("please_wait_el3").remove();
                });
            },
                error => {
                    //connection.mockError(new Error('Email or password is incorrect'));
                }
            );

    }
    ngAfterViewInit() {
        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js'/*, 'assets/demo/default/custom/components/charts/morris-charts.1.js'*/]);

    }

}