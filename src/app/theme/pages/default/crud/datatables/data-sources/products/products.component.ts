import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../../../helpers';
import { ScriptLoaderService } from '../../../../../../../_services/script-loader.service';
import { GlobalVars } from '../../../../../../../../app/app.component';


@Component({
    selector: "products",
    templateUrl: "./products.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ProductsComponent implements OnInit, AfterViewInit {

    public loggeduser2: any;

    constructor(private _script: ScriptLoaderService) {

    }
    ngOnInit() {
        this.loggeduser2 = GlobalVars.CurrentUser;// JSON.parse(localStorage.getItem("user"));
    }
    ngAfterViewInit() {
        this._script.loadScripts('products',
            ['assets/vendors/custom/datatables/datatables.bundle.js',
                'assets/demo/default/custom/crud/datatables/data-sources/products.js']);

    }

}