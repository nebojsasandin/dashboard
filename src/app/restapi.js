app.factory("RestApiClientService", ['$http', 'toaster',
    function($http, toaster) {
        
        var serviceBase = window.location.protocol + '//' + window.location.hostname + ":17022/DMCAProtectionWebAPI/key=476a1e02-cc3c-4d96-b55b-2604329a60b7/sometest";

        var obj = {};
        obj.toast = function(data) {
            taoster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        };
        obj.get = function(q) {
            return $http.get(serviceBase + q).then(function(results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            obj.post = function (q, object) {
                return $http.post(serviceBase + q, object).then(function(results) {
                    return results.data;
                }, function(results) {
                    var err = {status:"error", message:"An Internal Error Occured" };
                    return err;
                });
            };
        };
        obj.put = function(q, object) {
            return $http.put(serviceBase+q, object).then(function(results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function(results) {
                return results.data;
            })
        };
        return obj;
    }
])