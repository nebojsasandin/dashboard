import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from "./helpers";
import { HttpClient } from '@angular/common/http';

import { DataService } from './_services/data.service';
// import { Global } from './globals';

// import myGlobals = require('./globals');
// import * as myGlobals from './globals';
// declare global {
//     let WCFService: DataService;
// }

// declare let global: Global;
export namespace GlobalVars
{
    export let WCFService: DataService;
    export let CurrentUser: any;
    export let ClientProfile: any;
}

@Component({
    selector: 'body',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
    // _globalService = Global;
    title = 'app';
    globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';

    public messagee: string;

    constructor(private _router: Router, private _dataService: DataService) {
    }

    ngOnInit() {
        this._router.events.subscribe((route) => {
            if (route instanceof NavigationStart) {
                Helpers.setLoading(true);
                Helpers.bodyClass(this.globalBodyClass);
            }
            if (route instanceof NavigationEnd) {
                Helpers.setLoading(false);
            }
        });

        GlobalVars.WCFService = this._dataService;
        // this._globalService.setDataService(this._dataService);
        // this._globalService.WCFService = this._dataService;
        // global = new Global();
        // global.WCFService = this._dataService;
    }
}