var DatatablesDataSourceAjaxServer={init:function(){$("#m_table_1").DataTable({
        responsive:!0,
        searchDelay:1500,
        processing:!0,
        serverSide:!0,
        ajax: window.location.protocol + '//' + window.location.hostname + ":17022/DMCAProtectionWebAPI/dashboardapi/get/" + JSON.parse(localStorage.getItem("user")).apikey + "/getresults/" + JSON.parse(localStorage.getItem("user")).campid,
            columns:[
                {
                    data:"Index"
                },
                {
                    data:"Title"
                },
                {
                    data:"Url"
                },
                {
                    data:"DateFound"
                },
                {
                    data:"SubmittedDate"
                },
                {
                    data:"SubmittedStatus"
                }
                // {
                //     data:"RecordID"
                // },
                // {
                //     data:"OrderID"
                // },
                // {
                //     data:"Country"
                // },
                // {
                //     data:"ShipCity"
                // },
                // {
                //     data:"ShipAddress"
                // },
                // {
                //     data:"CompanyAgent"
                // },
                // {
                //     data:"CompanyName"
                // },
                // {
                //     data:"ShipDate"
                // },
                // {
                //     data:"Status"
                // },
                // {
                //     data:"Type"
                // },
                // {
                //     data:"TotalPayment"
                // }
            ],columnDefs:[
                {
                    targets:-1,
                    Title:"Status",
                    orderable:!1,
                    render:function(a,e,t,n)
                    {
                        {
                            var s={
                                1:{
                                    title:"Pending",
                                    class:"m-badge--brand"
                                },
                                0:{
                                    title:"-",
                                    class:" m-badge--metal"
                                },
                                6:{
                                    title:"Approved OLD",
                                    class:" m-badge--primary"
                                },
                                2:{
                                    title:"Approved",
                                    class:" m-badge--success"
                                },
                                4:{
                                    title:"Unknown",
                                    class:" m-badge--info"
                                },
                                3:{
                                    title:"Rejected",
                                    class:" m-badge--danger"
                                },
                                7:{
                                    title:"New",
                                    class:" m-badge--warning"
                                },
                                8:{
                                    title:"Can't Submit",
                                    class:" m-badge--warning"
                                },
                                12:{
                                    title:"Not in Index",
                                    class:" m-badge--primary"
                                },
                                13:{
                                    title:"Moved to DST",
                                    class:" m-badge--metal"
                                },
                            };
                            return void 0===s[a]?a:'<span class="m-badge '+s[a].class+' m-badge--wide">'+s[a].title+"</span>"
                        }
                    }
                },
                // {
                //     targets:-1,title:"Actions",orderable:!1,render:function(a,e,t,n)
                //     {
                //         return'\n                        <span class="dropdown">\n                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n                              <i class="la la-ellipsis-h"></i>\n                            </a>\n                            <div class="dropdown-menu dropdown-menu-right">\n                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\n                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\n                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\n                            </div>\n                        </span>\n                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">\n                          <i class="la la-edit"></i>\n                        </a>'
                //     }
                // },
                //============================
                // {
                //     targets:8,render:function(a,e,t,n){var s={1:{title:"Pending",class:"m-badge--brand"},2:{title:"Delivered",class:" m-badge--metal"},3:{title:"Canceled",class:" m-badge--primary"},4:{title:"Success",class:" m-badge--success"},5:{title:"Info",class:" m-badge--info"},6:{title:"Danger",class:" m-badge--danger"},7:{title:"Warning",class:" m-badge--warning"}};return void 0===s[a]?a:'<span class="m-badge '+s[a].class+' m-badge--wide">'+s[a].title+"</span>"}
                // },
                // {
                //     targets:9,render:function(a,e,t,n){var s={1:{title:"Online",state:"danger"},2:{title:"Retail",state:"primary"},3:{title:"Direct",state:"accent"}};return void 0===s[a]?a:'<span class="m-badge m-badge--'+s[a].state+' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-'+s[a].state+'">'+s[a].title+"</span>"}
                // }

    ]}
)}}; jQuery(document).ready(function () { DatatablesDataSourceAjaxServer.init() });